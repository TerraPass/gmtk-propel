﻿using UnityEngine;
using System;

using Propel.Audio;

namespace Propel.Gameplay
{
	public class Barrier : MonoBehaviour
	{
		[SerializeField]
		private AudioClip[] collisionClips;

		void OnCollisionEnter(Collision collision)
		{
			// TODO: Calculate reflection properly
			collision.rigidbody.velocity = transform.forward*collision.relativeVelocity.magnitude;

			AudioUtils.PlayOneOf(GetComponent<AudioSource>(), collisionClips);
		}
	}
}

