﻿using System;

namespace Propel.Gameplay
{
	// Everything in this file is stupid and needs to be refactored later.

	public enum DifficultyLevel
	{
		HARD 	= 0,
		MEDIUM 	= 1,
		EASY 	= 2
	}

	public struct DifficultyLevelRules
	{
		public float Time {get;}
		public int Energy {get;}

		public DifficultyLevelRules(float time, int energy)
		{
			this.Time = time;
			this.Energy = energy;
		}
	}

	public static class DifficultyLevelExtensions
	{
		private static readonly DifficultyLevelRules[] rules = new []{
			new DifficultyLevelRules(20f, 10),
			new DifficultyLevelRules(30f, 12),
			new DifficultyLevelRules(40f, 14)
		};

		public static DifficultyLevelRules GetRules(this DifficultyLevel difficultyLevel)
		{
			return rules[(int)difficultyLevel];
		}
	}
}

