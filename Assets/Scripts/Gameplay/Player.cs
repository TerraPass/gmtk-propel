﻿using UnityEngine;
using System;

using Terrapass.Extensions.Unity;
using Terrapass.Time;
using Terrapass.Debug;

namespace Propel.Gameplay
{
	public class Player : AbstractPlayer
	{
		private const float AIMING_LERP_AMOUNT = 1f;
		//private const float SHOOTING_COOLDOWN = 0.35f;

		[SerializeField]
		private int energyGainOnHit = 2;
		[SerializeField]
		private float timeLossOnHit = 1f;

		[SerializeField]
		private float timeGainOnChaserKill = 1f;
		[SerializeField]
		private float timeGainOnCirclerKill = 5f;

		[SerializeField]
		private float shootingCooldown = 0.25f;

		[SerializeField]
		private float propelForce = 5f;
		[SerializeField]
		private ForceMode propelForceMode = ForceMode.Impulse;

		[SerializeField]
		private float maxSpeed = 50f;

		[SerializeField]
		private Transform projectileSource;
		[SerializeField]
		private GameObject projectilePrefab;

		[SerializeField]
		private AbstractPlayerView view;

		[SerializeField]
		private GameObject spawnOnDeath;

		private Rigidbody myRigidbody;

		//private Vector3 aimingDirection;
		private Vector3 aimingTarget;

		private IResettableTimer shootingCooldownTimer;

		//[SerializeField]
		private LevelDirector director;

		private DifficultyLevelRules rules = DifficultyLevel.HARD.GetRules();

		private int energy = 12;
		private float time = 20f;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.myRigidbody = this.GetComponent<Rigidbody>();
			DebugUtils.Assert(this.myRigidbody != null, "Rigidbody component must be present");

			//this.aimingDirection = Vector3.forward;
			this.aimingTarget = Vector3.forward;

			this.shootingCooldownTimer = new ResettableExecutionTimer(false);

			this.energy = this.MaxEnergy;
			this.time = this.MaxTime;
		}

		void FixedUpdate()
		{
			DebugUtils.Assert(director != null, "LevelDirector must be set on Player before first FixedUpdate()");

			this.view.LookTarget = Vector3.Lerp(this.view.LookTarget, this.aimingTarget, AIMING_LERP_AMOUNT);

			// TODO: Maybe calculate time properly instead of decrementing it here
			this.time -= UnityEngine.Time.fixedDeltaTime;

			if(this.time <= 0)
			{
				Die();
			}
		}

		private void Die()
		{
			Instantiate(spawnOnDeath, transform.position, transform.rotation);

			this.director.OnPlayerKilled();

			this.gameObject.SetActive(false);
		}

		public override void AimAt(Vector3 point)
		{
			this.aimingTarget = point;
		}

		public override bool Shoot()
		{
			if(this.shootingCooldownTimer.ElapsedSeconds > this.shootingCooldown && energy > 0)
			{
				energy--;
				this.ShootImpl();
				this.shootingCooldownTimer.Reset(false);

				this.view.OnShot();

				return true;
			}
			else
			{
				if(energy <= 0)
				{
					this.view.OnNotEnoughEnergy();
				}
				return false;
			}
		}

		private void ShootImpl()
		{
			// Spawn projectile
			// TODO: Also make projectile affected by initial speed?
			var projectile = ((GameObject)Instantiate(this.projectilePrefab, this.projectileSource.position, this.projectileSource.rotation))
				.GetComponent<IProjectile>();
			DebugUtils.Assert(projectile != null, "IProjectile component must be present on projectile prefab");

			projectile.InitialVelocity = myRigidbody.velocity;

			// Propel the player
			this.myRigidbody.AddRelativeForce(propelForce*Vector3.back, propelForceMode);

			// Ensure that the speed is within limits
			var squaredMaxSpeed = maxSpeed * maxSpeed;
			if(this.myRigidbody.velocity.sqrMagnitude > squaredMaxSpeed)
			{
				// Scale back the velocity
				this.myRigidbody.velocity = this.myRigidbody.velocity * (maxSpeed/this.myRigidbody.velocity.magnitude);
			}
		}

		public override void OnBodyHit()
		{
			this.time -= this.timeLossOnHit;

			this.view.OnBodyHit();
		}

		public override void OnShieldHit()
		{
			if(this.energy < this.MaxEnergy)
			{
				this.energy += this.energyGainOnHit;
			}

			this.view.OnShieldHit();
		}

		public override void OnEnemyKilled(EnemyKind kind)
		{
			this.time = Mathf.Min(
				this.time + (kind == EnemyKind.CHASER ? this.timeGainOnChaserKill : this.timeGainOnCirclerKill), 
				this.MaxTime
			);
		}

		public override void ResetRules(DifficultyLevelRules rules)
		{
			this.rules = rules;
			this.time = this.MaxTime;
			this.energy = this.MaxEnergy;
		}

		public override Transform Transform
		{
			get {
				return this.transform;
			}
		}

		public override Vector3 CurrentVelocity
		{
			get {
				return this.myRigidbody.velocity;
			}
		}

		public override Rigidbody Rigidbody
		{
			get {
				return this.myRigidbody;
			}
		}

		public override int MaxEnergy
		{
			get {
				return this.rules.Energy;
			}
		}

		public override float MaxTime
		{
			get {
				return this.rules.Time;
			}
		}

		public override int Energy
		{
			get {
				return this.energy;
			}
		}

		public override float Time
		{
			get {
				return this.time;
			}
		}

		public override LevelDirector LevelDirector
		{
			get {
				return this.director;
			}
			set {
				this.director = value;
			}
		}
//		public Vector3 AimingDirection
//		{
//			get {
//				return this.aimingDirection;
//			}
//		}
	}
}

