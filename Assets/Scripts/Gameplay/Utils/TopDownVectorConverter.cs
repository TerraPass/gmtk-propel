﻿using UnityEngine;
using System;

namespace Propel.Gameplay.Utils
{
	public class TopDownVectorConverter : IVectorConverter
	{
		private float baseY;

		public TopDownVectorConverter(float baseY = 0)
		{
			this.baseY = baseY;
		}

		public Vector3 ToVector3(Vector2 point)
		{
			return new Vector3(point.x, baseY, point.y);
		}

		public Vector2 ToVector2(Vector3 point)
		{
			return new Vector2(point.x, point.z);
		}
	}
}

