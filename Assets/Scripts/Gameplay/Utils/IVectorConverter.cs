﻿using UnityEngine;
using System;

namespace Propel.Gameplay.Utils
{
	public interface IVectorConverter
	{
		Vector3 ToVector3(Vector2 point);
		Vector2 ToVector2(Vector3 point);
	}
}

