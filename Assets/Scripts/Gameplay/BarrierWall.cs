﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public class BarrierWall : MonoBehaviour
	{
		[SerializeField]
		private GameObject barrierPrefab;

		[SerializeField]
		private Vector3 center;

		[SerializeField]
		private float size = 100;

		void Start()
		{			
			var initialBarrier = (GameObject)Instantiate(
				barrierPrefab,
				center + Vector3.forward * size,
				Quaternion.identity
			);
			initialBarrier.transform.LookAt(center);

			var circumference = 2 * Mathf.PI * size;
			var barrierWidth = initialBarrier.GetComponent<Collider>().bounds.size.x;

			//print(initialBarrier.GetComponent<Collider>().bounds.size);

			int barriersNeeded = Mathf.CeilToInt(circumference / barrierWidth);
			for(int i = 1; i < barriersNeeded; i++)
			{
				var barrier = (GameObject)Instantiate(
					barrierPrefab,
					center + Vector3.forward * size,
					Quaternion.identity
				);
				barrier.transform.RotateAround(center, Vector3.up, ((float)i / (float)barriersNeeded) * 360f);
				barrier.transform.LookAt(center);
			}
		}
	}
}

