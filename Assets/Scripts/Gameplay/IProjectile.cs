﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public interface IProjectile
	{
		LayerMask IgnoredLayers {get;set;}

		/// <summary>
		/// Flat bonus to the speed of the projectile.
		/// </summary>
		/// <value>The initial speed.</value>
		//float InitialSpeed { get; set; }
		Vector3 InitialVelocity {get;set;}
	}
}

