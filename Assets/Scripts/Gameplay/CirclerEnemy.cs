﻿using UnityEngine;
using System;

using Terrapass.Extensions.Unity;
using Terrapass.Debug;
using Terrapass.Time;

using Propel.Audio;

namespace Propel.Gameplay
{
	public class CirclerEnemy : MonoBehaviour, IEnemy
	{
		[SerializeField]
		private AbstractPlayer initialPlayer;

		[SerializeField]
		private Transform body;
		[SerializeField]
		private float bodyRotationSpeed = 180f;//Mathf.PI; Who the f uses degrees when programming?

		[SerializeField]
		private float desiredDistance = 20f;

		[SerializeField]
		private float circlingLimit = 2f;	// How close to approach to desired position before rotating it
		[SerializeField]
		private float circlingStep = 45f;	// in degrees

		[SerializeField]
		private float maxRelativeSpeed = 1.1f;
		[SerializeField]
		private float minSpeed = 15f;

		[SerializeField]
		private float steeringForce = 5f;
		[SerializeField]
		private ForceMode steeringForceMode = ForceMode.Acceleration;

		[SerializeField]
		private float shootingPeriod = 2.0f;

		[SerializeField]
		private float maxShootingDistance = 25f;

		[SerializeField]
		private GameObject projectilePrefab;

		[SerializeField]
		private Transform projectileSource;

		[SerializeField]
		private AudioClip[] shootingClips;

		private Rigidbody myRigidbody;
		private AudioSource myAudioSource;

		private IPlayer player;

		private Vector3 desiredOffset;

		private IResettableTimer shootingCooldownTimer;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.myRigidbody = this.GetComponent<Rigidbody>();
			DebugUtils.Assert(myRigidbody != null, "Rigidbody component must be present");
			this.myAudioSource = this.GetComponent<AudioSource>();
			DebugUtils.Assert(this.myAudioSource != null, "AudioSource component is required");

			if(this.player == null)
			{
				this.player = initialPlayer;
			}

			// Start with randomly rotated desired offset
			desiredOffset = RotateAroundY(desiredDistance*Vector3.forward, UnityEngine.Random.value*360);

			shootingCooldownTimer = new ResettableExecutionTimer(false);
		}

		void OnDestroy()
		{
			if(this.player != null)
			{
				this.player.OnEnemyKilled(EnemyKind.CIRCLER);
			}
		}

		void FixedUpdate()
		{
			// Rotate the body
			body.Rotate(Vector3.up, Time.fixedDeltaTime*bodyRotationSpeed);

			if(this.player == null)
			{
				return;
			}

			var positionDiff = (player.Transform.position + desiredOffset) - transform.position;

			// Rotate desired offset if too close
			if(positionDiff.sqrMagnitude < circlingLimit * circlingLimit)
			{
				desiredOffset = RotateAroundY(desiredOffset, circlingStep);
			}

			// Apply force in the direction of desired position
			// TODO: proportional to remaining distance?
			this.myRigidbody.AddForce(steeringForce * positionDiff.normalized, steeringForceMode);

			// Ensure that we aren't over speed limit
			var squaredMaxSpeed = maxRelativeSpeed*maxRelativeSpeed*player.CurrentVelocity.sqrMagnitude;
			var squaredMinSpeed = minSpeed * minSpeed;
			var sqrLimitSpeed = Mathf.Max(squaredMaxSpeed, squaredMinSpeed);
			if(myRigidbody.velocity.sqrMagnitude > sqrLimitSpeed)
			{
				var limitSpeed = Mathf.Sqrt(sqrLimitSpeed);
				// Scale back the velocity
				this.myRigidbody.velocity = this.myRigidbody.velocity * (limitSpeed/this.myRigidbody.velocity.magnitude);
			}

			// If cooldown has passed and we are within range, shoot
			if(shootingCooldownTimer.ElapsedSeconds > shootingPeriod 
				&& (player.Transform.position - transform.position).sqrMagnitude < maxShootingDistance*maxShootingDistance)
			{
				this.ShootImpl();
				this.shootingCooldownTimer.Reset(false);
			}
		}

		private void ShootImpl()
		{
			// Shoot at an angle to the player
			this.projectileSource.LookAt(player.Transform);
			this.projectileSource.Rotate(Vector3.up, 90f);
			//this.projectileSource.Rotate(Vector3.up, 90f - 180f*UnityEngine.Random.value);

			// Shoot straight at the player
			//this.projectileSource.LookAt(player.Transform);

			// Spawn projectile
			var projectile = ((GameObject)Instantiate(
				this.projectilePrefab, 
				this.projectileSource.position, 
				this.projectileSource.rotation
			)).GetComponent<IHomingProjectile>();

			DebugUtils.Assert(projectile != null, "projectilePrefab must have an IHomingProjectile component");

			//projectile.InitialSpeed = myRigidbody.velocity.magnitude;
			projectile.Target = this.player.Transform;
			projectile.TargetRigidbody = this.player.Rigidbody;

			AudioUtils.PlayOneOf(myAudioSource, shootingClips);
		}

		public IPlayer Player
		{
			get {
				return this.player;
			}
			set {
				this.player = value;
			}
		}

		private static Vector3 RotateAroundY(Vector3 initialVector, float angle)
		{
			return Quaternion.Euler(0, angle, 0) * initialVector;
		}
	}
}

