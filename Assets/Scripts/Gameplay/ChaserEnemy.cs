﻿using UnityEngine;
using System;

using Terrapass.Time;
using Terrapass.Debug;
using Terrapass.Extensions.Unity;

using Propel.Audio;

namespace Propel.Gameplay
{
	public class ChaserEnemy : MonoBehaviour, IEnemy
	{
		// That's what you get instead of FSM when the deadline approaches
		private enum Mode
		{
			//IDLE 	= 0,
			CHASE 	= 1,
			FLEE 	= 2
		}

		[SerializeField]
		private AbstractPlayer initialPlayer;

		[SerializeField]
		private float maxFleeDistance = 20f;
		[SerializeField]
		private float minChaseDistance = 5f;

		[SerializeField]
		private float steeringForce = 5f;
		[SerializeField]
		private ForceMode steeringForceMode = ForceMode.Acceleration;

		//[SerializeField]
		//private float maxSpeed = 35f;

		[SerializeField]
		private float minSpeed = 5f;	// Min speed - enemies are allowed to attain this speed even if the player is stationary
		[SerializeField]
		private float maxRelativeSpeed = 1.5f;	// Max speed, relative to player (i.e. 1.5 -> 1.5 times the player speed)

		[SerializeField]
		private float shootingPeriod = 0.5f;

		[SerializeField]
		private float maxShootingDistance = 30f;

		[SerializeField]
		private GameObject projectilePrefab;

		[SerializeField]
		private Transform projectileSource;

		[SerializeField]
		private AudioClip[] shootingClips;

		private IPlayer player;

		private Rigidbody myRigidbody;
		private AudioSource myAudioSource;

		private IResettableTimer shootingCooldownTimer;

		private Mode currentMode;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			if(this.player == null)
			{
				this.player = initialPlayer;
			}

			this.myRigidbody = this.GetComponent<Rigidbody>();
			DebugUtils.Assert(this.myRigidbody != null, "Rigidbody component is required");
			this.myAudioSource = this.GetComponent<AudioSource>();
			DebugUtils.Assert(this.myAudioSource != null, "AudioSource component is required");
			this.shootingCooldownTimer = new ResettableExecutionTimer(false);
			this.currentMode = Mode.CHASE;
		}

		void OnDestroy()
		{
			if(this.player != null)
			{
				this.player.OnEnemyKilled(EnemyKind.CHASER);
			}
		}

		void FixedUpdate()
		{
			if(this.player == null)
			{
				return;
			}

			if(this.currentMode == Mode.CHASE)
			{
				var positionDiff = player.Transform.position - transform.position;

				// Apply force in the direction of the player
				this.myRigidbody.AddForce(steeringForce * positionDiff.normalized, steeringForceMode);

				var squaredMinDistance = minChaseDistance * minChaseDistance;
				if(positionDiff.sqrMagnitude < squaredMinDistance)
				{
					this.currentMode = Mode.FLEE;

					// Add random "detour" force to prevent all of the chasers from merging
					// (this is faster than implementing proper steering behaviours for separation and cohesion)
					var detour = UnityEngine.Random.onUnitSphere*10f;
					this.myRigidbody.AddForce(steeringForce*detour, steeringForceMode);
				}

				// If cooldown has passed and we are in range, shoot
				if(shootingCooldownTimer.ElapsedSeconds > shootingPeriod
					&& positionDiff.sqrMagnitude < maxShootingDistance*maxShootingDistance)
				{
					this.ShootImpl();
					this.shootingCooldownTimer.Reset(false);
				}
			}
			else 	// FLEE
			{
				var positionDiff = player.Transform.position - transform.position;

				// Apply force in the direction of the player
				this.myRigidbody.AddForce(-steeringForce * positionDiff.normalized, steeringForceMode);

				var squaredMaxDistance = maxFleeDistance * maxFleeDistance;
				if(positionDiff.sqrMagnitude > squaredMaxDistance)
				{
					this.currentMode = Mode.CHASE;

					// Add random "detour" force to prevent all of the chasers from merging
					// (this is faster than implementing proper steering behaviours for separation and cohesion)
					var detour = UnityEngine.Random.onUnitSphere*10f;
					this.myRigidbody.AddForce(steeringForce*detour, steeringForceMode);
				}
			}

			// Align the vehicle model with velocity
			this.transform.LookAt(transform.position + myRigidbody.velocity);

			// Ensure that we are below max speed limit
			var squaredMaxSpeed = maxRelativeSpeed*maxRelativeSpeed*player.CurrentVelocity.sqrMagnitude;
			var squaredMinSpeed = minSpeed * minSpeed;
			var sqrLimitSpeed = Mathf.Max(squaredMaxSpeed, squaredMinSpeed);
			if(myRigidbody.velocity.sqrMagnitude > sqrLimitSpeed)
			{
				var limitSpeed = Mathf.Sqrt(sqrLimitSpeed);
				// Scale back the velocity
				this.myRigidbody.velocity = this.myRigidbody.velocity * (limitSpeed/this.myRigidbody.velocity.magnitude);
			}
		}

		private void ShootImpl()
		{
			// Aim at player
			// FIXME: This is not how leading works!
			this.projectileSource.LookAt(this.Player.Transform.position/* + this.Player.CurrentVelocity*/);

			// Spawn projectile
			var projectile = ((GameObject)Instantiate(
				this.projectilePrefab, 
				this.projectileSource.position, 
				this.projectileSource.rotation
			)).GetComponent<IProjectile>();

			DebugUtils.Assert(projectile != null, "projectilePrefab must have an IProjectile component");

			//projectile.InitialSpeed = myRigidbody.velocity.magnitude;
			projectile.InitialVelocity = myRigidbody.velocity;

			AudioUtils.PlayOneOf(myAudioSource, shootingClips);
		}

		public IPlayer Player
		{
			get {
				return this.player;
			}
			set {
				this.player = value;
			}
		}
	}
}

