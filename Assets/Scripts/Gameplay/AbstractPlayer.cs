﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public abstract class AbstractPlayer : MonoBehaviour, IPlayer
	{
		public abstract int MaxEnergy {get;}
		public abstract float MaxTime {get;}

		public abstract int Energy {get;}
		public abstract float Time {get;}

		//public abstract void AimAt(Vector2 point);
		public abstract void AimAt(Vector3 point);

		public abstract void OnBodyHit();
		public abstract void OnShieldHit();

		public abstract void OnEnemyKilled(EnemyKind kind);

		public abstract bool Shoot();

		public abstract Transform Transform { get;}

		public abstract Vector3 CurrentVelocity {get;}

		public abstract Rigidbody Rigidbody {get;}

		public abstract LevelDirector LevelDirector {get; set;}

		public abstract void ResetRules(DifficultyLevelRules rules);
	}
}

