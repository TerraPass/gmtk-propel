﻿using UnityEngine;
using System;

using Terrapass.Debug;
using Terrapass.Extensions.Unity;

using Terrapass.Time;

using Propel.Ui;

namespace Propel.Gameplay
{
	public class LevelDirector : MonoBehaviour
	{
		private const string HIGHSCORE_PREFS_KEY_PREFIX = "highscore_";

		[SerializeField]
		private AbstractPlayer player;

		[SerializeField]
		private Canvas tutorialCanvas;

		[SerializeField]
		private PlayerHud playerHud;
		[SerializeField]
		private GameOverUi gameOverUi;
		[SerializeField]
		private PauseMenu pauseMenu;

		[SerializeField]
		private MusicPlayer musicPlayer;

		[SerializeField]
		private float secondsBeforeGameOverScreen = 3f;

		private bool tutorial;
		private bool gameOver;

		private ITimer survivalTimer;

		private ITimer gameOverTimer;

		private DifficultyLevel difficultyLevel = DifficultyLevel.HARD;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			Time.timeScale = 0f;

			this.tutorial = true;

			this.gameOver = false;
			this.gameOverTimer = new ExecutionTimer(true);

			this.playerHud.Visible = true;
			//this.gameOverUi.Visible = false;
			this.pauseMenu.Visible = false;

			this.survivalTimer = new ExecutionTimer(true);

			this.player.LevelDirector = this;
		}

		void Update()
		{
			if(tutorial)
			{
				return;
			}

			if(!gameOver)
			{
				if(Input.GetKeyDown(KeyCode.Escape))
				{
					TogglePause();
				}
			}
			else
			{
				if(!this.gameOverTimer.IsPaused && this.gameOverTimer.ElapsedSeconds > secondsBeforeGameOverScreen)
				{
					// Pause but don't display pause menu
					this.Pause(false);

					this.playerHud.Visible = false;
					this.pauseMenu.Visible = false;

					var bestTime = PlayerPrefs.GetFloat(HIGHSCORE_PREFS_KEY_PREFIX + difficultyLevel.ToString());

					this.gameOverUi.Show(this.survivalTimer.ElapsedSeconds, bestTime, this.difficultyLevel);

					if(this.survivalTimer.ElapsedSeconds > bestTime)
					{
						PlayerPrefs.SetFloat(HIGHSCORE_PREFS_KEY_PREFIX + difficultyLevel.ToString(), this.survivalTimer.ElapsedSeconds);
					}

					this.gameOverTimer.Pause();
				}
			}
		}

		public void OnPlayerKilled()
		{
			this.gameOver = true;
			this.playerHud.Visible = false;
			this.gameOverTimer.Resume();
			this.survivalTimer.Pause();
			this.musicPlayer.Stop();
		}

		public void OnDifficultyLevelSelected(DifficultyLevel difficultyLevel)
		{
			this.difficultyLevel = difficultyLevel;

			this.player.ResetRules(difficultyLevel.GetRules());

			this.playerHud.OnGameStarted();

			this.tutorialCanvas.gameObject.SetActive(false);
			this.musicPlayer.Play();
			this.tutorial = false;

			this.Unpause();
		}

		private void Pause(bool showMenu = true)
		{
			DebugUtils.Assert(!IsPaused, "Pause() must not be called if already paused");

			Time.timeScale = 0f;

			this.pauseMenu.Visible = showMenu || this.pauseMenu.Visible;
			this.survivalTimer.Pause();

			// Let cursor leave the window, when the game is paused
			Cursor.lockState = CursorLockMode.None;
		}

		private void Unpause()
		{
			DebugUtils.Assert(IsPaused, "Unpause() must not be called if not paused");

			Time.timeScale = 1f;

			this.pauseMenu.Visible = false;
			this.survivalTimer.Resume();

			// Confine cursor to the game window
			Cursor.lockState = CursorLockMode.Confined;
		}

		public bool IsPaused
		{
			get {
				return Time.timeScale < 0.1f;
			}
		}

		private void TogglePause()
		{
			if(IsPaused)
			{
				this.Unpause();
			}
			else
			{
				this.Pause();
			}
		}

		public bool Tutorial
		{
			get {
				return this.tutorial;
			}
		}
	}
}

