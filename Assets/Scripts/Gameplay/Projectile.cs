﻿using UnityEngine;
using System;

using Terrapass.Time;
using Terrapass.Debug;

namespace Propel.Gameplay
{
	public class Projectile : AbstractProjectile
	{
		[SerializeField]
		private LayerMask ignoredLayers;

		[SerializeField]
		private float speed = 5f;

		[SerializeField]
		private float lifetime = 3f;

		[SerializeField]
		private GameObject spawnOnDeath;

		private Rigidbody myRigidbody;

		private ITimer lifetimeTimer;

		void Start()
		{
			this.myRigidbody = this.GetComponent<Rigidbody>();
			DebugUtils.Assert(this.myRigidbody != null, "Rigidbody component must be present");

			this.lifetimeTimer = new ExecutionTimer(false);

			// Setup forward movement
			this.myRigidbody.velocity = InitialVelocity + transform.forward*speed;
		}

		void FixedUpdate()
		{
			// Check if it's time to die
			if(this.lifetimeTimer.ElapsedSeconds > this.lifetime)
			{
				Die();
				return;
			}

			// Move forward
			//this.transform.Translate((InitialSpeed + speed)*Time.fixedDeltaTime*Vector3.forward);
		}

		private void Die()
		{
			Instantiate(this.spawnOnDeath, transform.position, transform.rotation);
			Destroy(this.gameObject);
		}

		void OnCollisionEnter(Collision collision)
		{
			Die();
		}

		public override LayerMask IgnoredLayers
		{
			get {
				return this.ignoredLayers;
			}
			set {
				this.ignoredLayers = value;
			}
		}

		//public override float InitialSpeed { get; set; }
		public override Vector3 InitialVelocity { get; set; }
	}
}

