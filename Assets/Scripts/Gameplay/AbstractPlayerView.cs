﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public abstract class AbstractPlayerView : MonoBehaviour, IPlayerView
	{
		//public abstract Vector2 LookDirection { get; set; }
		public abstract Vector3 LookTarget { get; set; }

		public abstract void OnShieldHit();
		public abstract void OnBodyHit();

		public abstract void OnShot();
		public abstract void OnNotEnoughEnergy();
	}
}

