﻿using UnityEngine;
using System;

using Terrapass.Time;
using Terrapass.Debug;

namespace Propel.Gameplay
{
	public class HomingProjectile : MonoBehaviour, IHomingProjectile
	{
		[SerializeField]
		private Transform target;
		[SerializeField]
		private Rigidbody targetRigidbody;

		[SerializeField]
		private LayerMask ignoredLayers;
		[SerializeField]
		private float initialSpeed = 5f;

		[SerializeField]
		private float maxRelativeSpeed = 2.0f;
		[SerializeField]
		private float minSpeed = 5f;

		[SerializeField]
		private float lifetime = 10f;

		[SerializeField]
		private float homingForce = 7.5f;
		[SerializeField]
		private ForceMode homingForceMode = ForceMode.Acceleration;

		[SerializeField]
		private GameObject spawnOnDeath;

		private Collider myCollider;
		private Rigidbody myRigidbody;

		private ITimer lifetimeTimer;

		void Start()
		{
			this.myCollider = this.GetComponent<Collider>();
			DebugUtils.Assert(this.myCollider != null, "Collider component must be present");
			this.myRigidbody = this.GetComponent<Rigidbody>();
			DebugUtils.Assert(this.myRigidbody != null, "Rigidbody component must be present");

			this.lifetimeTimer = new ExecutionTimer(false);

			// Apply initial speed
			this.myRigidbody.velocity = this.transform.forward*initialSpeed + this.InitialVelocity;
		}

		void FixedUpdate()
		{
			// Check if it's time to die
			if(this.lifetimeTimer.ElapsedSeconds > this.lifetime)
			{
				Die();
				return;
			}

			// Home in on the target
			var positionDiff = target.transform.position - transform.position;
			this.myRigidbody.AddForce(homingForce * positionDiff, homingForceMode);

			// Ensure that the speed is within limits
//			var squaredMaxSpeed = maxSpeed * maxSpeed;
//			if(this.myRigidbody.velocity.sqrMagnitude > squaredMaxSpeed)
//			{
//				// Scale back the velocity
//				this.myRigidbody.velocity = this.myRigidbody.velocity * (maxSpeed/this.myRigidbody.velocity.magnitude);
//			}
			// Ensure that we aren't over speed limit
			var squaredMaxSpeed = maxRelativeSpeed*maxRelativeSpeed*targetRigidbody.velocity.sqrMagnitude;
			var squaredMinSpeed = minSpeed * minSpeed;
			var sqrLimitSpeed = Mathf.Max(squaredMaxSpeed, squaredMinSpeed);
			if(myRigidbody.velocity.sqrMagnitude > sqrLimitSpeed)
			{
				var limitSpeed = Mathf.Sqrt(sqrLimitSpeed);
				// Scale back the velocity
				this.myRigidbody.velocity = this.myRigidbody.velocity * (limitSpeed/this.myRigidbody.velocity.magnitude);
			}
		}

		private void Die()
		{
			Instantiate(this.spawnOnDeath, transform.position, transform.rotation);
			Destroy(this.gameObject);
		}

		void OnCollisionEnter(Collision collision)
		{
			//Debug.LogFormat("{0} collided with {1}", this.name, collision.gameObject.name);
			Die();
//			if(ignoredLayers.value == collision.gameObject.layer)
//			{
//				Physics.IgnoreCollision(this.myCollider, collision.collider);
//			}
		}

		public Transform Target
		{
			get {
				return this.target;
			}
			set {
				this.target = value;
			}
		}

		public Rigidbody TargetRigidbody
		{
			get {
				return this.targetRigidbody;
			}
			set {
				this.targetRigidbody = value;
			}
		}

		public LayerMask IgnoredLayers
		{
			get {
				return this.ignoredLayers;
			}
			set {
				this.ignoredLayers = value;
			}
		}

//		public float InitialSpeed
//		{
//			get {
//				return this.initialSpeed;
//			}
//			set {
//				this.initialSpeed = value;
//			}
//		}

		public Vector3 InitialVelocity
		{
			get;
			set;
		}
	}
}

