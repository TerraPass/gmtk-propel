﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public interface IHomingProjectile : IProjectile
	{
		Transform Target {get; set;}
		Rigidbody TargetRigidbody { get; set;}
	}
}

