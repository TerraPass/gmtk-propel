﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public interface IPlayer
	{
		int MaxEnergy {get;}
		float MaxTime {get;}

		int Energy {get;}
		float Time {get;}

		//void AimAt(Vector2 point);
		void AimAt(Vector3 point);

		bool Shoot();

		void OnBodyHit();
		void OnShieldHit();

		void OnEnemyKilled(EnemyKind kind);

		Transform Transform {get;}
		Vector3 CurrentVelocity {get;}

		// FIXME: Don't expose this maybe?
		Rigidbody Rigidbody {get;}

		LevelDirector LevelDirector {get;set;}

		void ResetRules(DifficultyLevelRules rules);
	}
}

