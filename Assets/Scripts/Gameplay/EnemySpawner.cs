﻿using UnityEngine;
//using System;
using System.Collections.Generic;

using Terrapass.Time;

namespace Propel.Gameplay
{
	public class EnemySpawner : MonoBehaviour
	{
		[SerializeField]
		private AbstractPlayer player;

		[SerializeField]
		private GameObject chaserPrefab;
		[SerializeField]
		private GameObject circlerPrefab;

		[SerializeField]
		private Vector3 center;
		[SerializeField]
		private float radius;

		[SerializeField]
		private float spawnPointsDistance = 20f;

		[SerializeField]
		private float spawnPeriod = 20f;

		[SerializeField]
		private float spawnProbability = 0.25f;

		[SerializeField]
		private float chaserProbability = 0.6f;

		private IList<Vector3> spawnLocations;

		private IResettableTimer spawnTimer;

		void Start()
		{	
			var circumference = 2 * Mathf.PI * radius;

			int locationsNeeded = Mathf.CeilToInt(circumference / spawnPointsDistance);
			this.spawnLocations = new List<Vector3>(locationsNeeded);
			for(int i = 0; i < locationsNeeded; i++)
			{
				this.spawnLocations.Add(
					center + Quaternion.AngleAxis(((float)i / (float)locationsNeeded) * 360f, Vector3.up)*Vector3.forward*radius
				);
			}

			this.spawnTimer = new ResettableExecutionTimer(false);
		}

		void FixedUpdate()
		{
			if(this.spawnTimer.ElapsedSeconds > this.spawnPeriod)
			{
				foreach(var spawnLocation in spawnLocations)
				{
					if(Random.value < spawnProbability)
					{
						bool spawnChaser = (Random.value < chaserProbability);

						var enemy = ((GameObject)Instantiate(
							spawnChaser ? chaserPrefab : circlerPrefab,
							spawnLocation,
							Quaternion.identity
						)).GetComponent<IEnemy>();

						enemy.Player = player;
					}
				}

				this.spawnTimer.Reset(false);
			}
		}
	}
}

