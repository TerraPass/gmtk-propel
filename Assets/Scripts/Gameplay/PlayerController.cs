﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public class PlayerController : MonoBehaviour
	{
		[SerializeField]
		private LevelDirector director;

		[SerializeField]
		private AbstractPlayer player;
		[SerializeField]
		private Camera relativeToCamera;

		void Start()
		{

		}

		void Update()
		{
			if(director.IsPaused)
			{
				return;
			}

			// Aim at the point the user points to
			var screenPoint = Input.mousePosition;
			// FIXME: This only works for topdown camera
			var worldPoint = this.relativeToCamera.ScreenToWorldPoint(
				new Vector3(screenPoint.x, screenPoint.y, relativeToCamera.transform.position.y)
			);
			//print(worldPoint);
			this.player.AimAt(worldPoint);

			// Fire on LMB down
			if(Input.GetMouseButtonDown(0))
			{
				this.player.Shoot();
			}
		}
	}
}

