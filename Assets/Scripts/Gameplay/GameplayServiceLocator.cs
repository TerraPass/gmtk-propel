﻿using UnityEngine;
using System;

using Terrapass.Extensions.Unity;

using Propel.Gameplay.Utils;

namespace Propel.Gameplay
{
	public class GameplayServiceLocator : MonoBehaviour
	{
		private IVectorConverter vectorConverter;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.vectorConverter = new TopDownVectorConverter();
		}

		public IVectorConverter VectorConverter
		{
			get {
				return this.vectorConverter;
			}
		}
	}
}

