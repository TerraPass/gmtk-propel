﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Terrapass.Debug;
using Terrapass.Time;

using Propel.Audio;

namespace Propel.Gameplay
{
	public class PlayerView : AbstractPlayerView
	{
		[SerializeField]
		private AudioClip[] absorbClips;
		[SerializeField]
		private AudioClip[] hurtClips;
		[SerializeField]
		private AudioClip[] shotClips;
		[SerializeField]
		private AudioClip[] noEnergyClips;

		[SerializeField]
		private float shieldAbsorbTime = 0.5f;

		[SerializeField]
		private Material shieldDefault;
		[SerializeField]
		private Material shieldActive;

		[SerializeField]
		private Renderer shieldRenderer;

//		private Vector2 lookDirection;
		private Vector3 lookTarget;

		private AudioSource myAudioSource;

		private IResettableTimer shieldAbsorbTimer;

		void Start()
		{
			this.myAudioSource = this.GetComponent<AudioSource>();
			DebugUtils.Assert(this.myAudioSource != null, "AudioSource must be present");

			this.shieldAbsorbTimer = new ResettableExecutionTimer(true);
		}

		void Update()
		{
			if(this.shieldAbsorbTimer.ElapsedSeconds > shieldAbsorbTime)
			{
				this.shieldRenderer.material = shieldDefault;
				this.shieldAbsorbTimer.Reset(true);
			}
		}

//		public override Vector2 LookDirection
//		{
//			get {
//				return this.lookDirection;
//			}
//			set {
//				this.lookDirection = value;
//			}
//		}

		public override Vector3 LookTarget
		{
			get {
				return this.lookTarget;
			}
			set {
				this.lookTarget = value;
				this.transform.LookAt(this.lookTarget);
			}
		}

		public override void OnShieldHit()
		{
			AudioUtils.PlayOneOf(myAudioSource, absorbClips);

			this.shieldRenderer.material = this.shieldActive;

			this.shieldAbsorbTimer.Reset(false);
		}

		public override void OnBodyHit()
		{
			AudioUtils.PlayOneOf(myAudioSource, hurtClips);
		}

		public override void OnShot()
		{
			AudioUtils.PlayOneOf(myAudioSource, shotClips);
		}

		public override void OnNotEnoughEnergy()
		{
			AudioUtils.PlayOneOf(myAudioSource, noEnergyClips);
		}
	}
}
