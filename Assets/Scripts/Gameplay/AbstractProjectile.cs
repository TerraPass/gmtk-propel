﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public abstract class AbstractProjectile : MonoBehaviour, IProjectile
	{
		public abstract LayerMask IgnoredLayers { get; set; }

		//public abstract float InitialSpeed {get;set;}
		public abstract Vector3 InitialVelocity {get;set;}
	}
}

