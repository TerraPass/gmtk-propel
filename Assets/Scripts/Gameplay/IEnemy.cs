﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public enum EnemyKind
	{
		CHASER 		= 0,
		CIRCLER 	= 1
	}

	public interface IEnemy
	{
		IPlayer Player { get; set; }
	}
}

