﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public class PlayerHitDetector : MonoBehaviour
	{
//		private enum HitKind
//		{
//			BODY = 0,
//			SHIELD = 1
//		}

		[SerializeField]
		private float bodyShieldThreshold = 0.0f;

		[SerializeField]
		private AbstractPlayer player;
//		[SerializeField]
//		private HitKind hitKind;
		[SerializeField]
		private LayerMask hostileProjectilesLayer;

		void OnCollisionEnter(Collision collision)
		{
			//Debug.Log("Hit detector detected collision");
			if((1 << collision.gameObject.layer) == hostileProjectilesLayer.value)
			{
				var localCollisionPoint = transform.InverseTransformPoint(collision.contacts[0].point);

				if(localCollisionPoint.z < bodyShieldThreshold)
				{
					player.OnShieldHit();
				}
				else
				{
					player.OnBodyHit();
				}
				//Debug.Log(transform.InverseTransformPoint(collision.contacts[0].point));
				//player.OnBodyHit();
			}
		}
	}
}

