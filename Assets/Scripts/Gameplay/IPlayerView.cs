﻿using UnityEngine;
using System;

namespace Propel.Gameplay
{
	public interface IPlayerView
	{
		//Vector2 LookDirection { get; set; }
		Vector3 LookTarget {get; set; }

		void OnShieldHit();
		void OnBodyHit();

		void OnShot();
		void OnNotEnoughEnergy();
	}
}

