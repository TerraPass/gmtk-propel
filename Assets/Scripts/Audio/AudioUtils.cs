﻿using UnityEngine;

namespace Propel.Audio
{
	public static class AudioUtils
	{
		public static void PlayOneOf(AudioSource source, AudioClip[] clips)
		{
			source.PlayOneShot(clips[UnityEngine.Random.Range(0, clips.Length)]);
		}
	}
}
