﻿using UnityEngine;
using System;

using Terrapass.Debug;

namespace Propel.Cameras
{
	public class HardTopdownCamera : MonoBehaviour
	{
		[SerializeField]
		private Transform target;
		// TODO: Calculate displacementTolerance dynamically, based on camera (screen) dimensions.
		[SerializeField]
		private float distanceTolerance = 5f;

		private Camera myCamera;

		void Start()
		{
			this.myCamera = this.GetComponent<Camera>();
			DebugUtils.Assert(this.myCamera != null, "Camera component must be present");
		}

		void Update()
		{
			if(this.target != null)
			{
				var correctedTargetPosition = new Vector3(target.position.x, transform.position.y, target.position.z);

				var squaredTolerance = distanceTolerance * distanceTolerance;
				var squaredDistance = Vector3.SqrMagnitude(correctedTargetPosition - transform.position);

				// If the target moved out of tolerated range
				if(squaredDistance > squaredTolerance)
				{
					var distance = Mathf.Sqrt(squaredDistance);

					// Move as much closer to the target as necessary for its return within tolerance range
					this.transform.position += (correctedTargetPosition - this.transform.position) * ((distance - distanceTolerance)/distance);
				}
			}
		}
	}
}

