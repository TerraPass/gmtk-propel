using UnityEngine;

using Terrapass.Extensions.Unity;

namespace Propel.Cameras
{
    public class ParallaxBackground : MonoBehaviour
    {
        [SerializeField]
        private Transform relativeToCamera;

        [SerializeField]
        private Transform nearBackground;
        [SerializeField]
        private Transform farBackground;

        [SerializeField]
        private float nearRelativeMovement = 0.5f;
        [SerializeField]
        private float farRelativeMovement = 0.05f;

        // [SerializeField]
        private Vector3 origin = Vector3.zero;

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            // Origin is the initial camera position without the Y coordinate.
            this.origin = XZProjection(this.relativeToCamera.position);

            // this.nearBackground.position = new Vector3(origin.x, nearBackground.position.y, origin.z);
            // this.farBackground.position = new Vector3(origin.x, farBackground.position.y, origin.z);
        }

        void Update()
        {
            var currentCameraPosition = XZProjection(this.relativeToCamera.position);
            var currentCameraDisplacement = currentCameraPosition - origin;

            var nearDisplacement = currentCameraPosition + (-nearRelativeMovement)*currentCameraDisplacement;
            var farDisplacement = currentCameraPosition + (-farRelativeMovement)*currentCameraDisplacement;

            this.nearBackground.position = new Vector3(
                nearDisplacement.x,
                nearBackground.position.y,
                nearDisplacement.z
            );
            this.farBackground.position = new Vector3(
                farDisplacement.x,
                farBackground.position.y,
                farDisplacement.z
            );
        }

        private static Vector3 XZProjection(Vector3 vector)
        {
            return vector - (vector.y*Vector3.up);
        }
    }
}