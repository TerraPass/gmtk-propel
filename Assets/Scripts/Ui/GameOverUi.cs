﻿using UnityEngine;
using UnityEngine.UI;
using System;

using Terrapass.Extensions.Unity;

using Propel.Gameplay;

namespace Propel.Ui
{
	public class GameOverUi : MonoBehaviour
	{
		private const string YOUR_TIME_MESSAGE_TEMPLATE = "You survived for {0} seconds on {1} difficulty!";
		private const string BEST_TIME_MEESAGE_TEMPLATE = "Best time: {0} seconds.";

		[SerializeField]
		private Canvas canvas;

		[SerializeField]
		private Text yourTimeText;
		[SerializeField]
		private Text bestTimeText;
		[SerializeField]
		private Text newHighscoreText;

		[SerializeField]
		private Button restartButton;
		[SerializeField]
		private Button quitButton;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			restartButton.onClick.AddListener(() => Application.LoadLevel(Application.loadedLevel));
			quitButton.onClick.AddListener(() => Application.Quit());
		}

		public void Show(float yourTime, float bestTime, DifficultyLevel difficultyLevel)
		{
			this.yourTimeText.text = string.Format(
				YOUR_TIME_MESSAGE_TEMPLATE, 
				Mathf.RoundToInt(yourTime),
				difficultyLevel.ToString().ToLowerInvariant()
			);
			this.bestTimeText.text = string.Format(
				BEST_TIME_MEESAGE_TEMPLATE,
				Mathf.RoundToInt(bestTime)
			);

			this.newHighscoreText.gameObject.SetActive(yourTime > bestTime);

			this.canvas.enabled = true;
		}
	}
}

