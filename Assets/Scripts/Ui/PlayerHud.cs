﻿using UnityEngine;
using UnityEngine.UI;
using System;

using Terrapass.Extensions.Unity;

using Propel.Gameplay;

namespace Propel.Ui
{
	public class PlayerHud : MonoBehaviour
	{
		[SerializeField]
		private AbstractPlayer player;

		[SerializeField]
		private Canvas canvas;

		[SerializeField]
		private BarIndicator energyIndicator;
		[SerializeField]
		private BarIndicator timeIndicator;
		[SerializeField]
		private Text timeText;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			// The timer is hidden initially to not mislead the player
			this.timeText.enabled = false;
		}

		void Update()
		{
			if(player == null)
			{
				return;
			}

			var time = Mathf.Max(player.Time, 0f);

			this.energyIndicator.Value = (float)player.Energy / (float)player.MaxEnergy;
			this.timeIndicator.Value = time / player.MaxTime;
			this.timeText.text = string.Format("{0}", Mathf.CeilToInt(time));
		}

		public bool Visible
		{
			get {
				return this.canvas.gameObject.activeSelf;
			}
			set {
				this.canvas.gameObject.SetActive(value);
			}
		}

		public void OnGameStarted()
		{
			// Show timer
			this.timeText.enabled = true;
		}
	}
}

