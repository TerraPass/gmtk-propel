using UnityEngine;
using UnityEngine.UI;
using System;

using Terrapass.Extensions.Unity;

using Propel.Gameplay;

namespace Propel.Ui
{
    public class DifficultySelector : MonoBehaviour
    {
        [SerializeField]
        private LevelDirector levelDirector;

        [SerializeField]
        private Button hardButton;
        [SerializeField]
        private Button mediumButton;
        [SerializeField]
        private Button easyButton;

        void Start()
        {
            this.EnsureRequiredFieldsAreSetInEditor();

            this.hardButton.onClick.AddListener(
                () => levelDirector.OnDifficultyLevelSelected(DifficultyLevel.HARD)
            );
            this.mediumButton.onClick.AddListener(
                () => levelDirector.OnDifficultyLevelSelected(DifficultyLevel.MEDIUM)
            );
            this.easyButton.onClick.AddListener(
                () => levelDirector.OnDifficultyLevelSelected(DifficultyLevel.EASY)
            );
        }
    }
}