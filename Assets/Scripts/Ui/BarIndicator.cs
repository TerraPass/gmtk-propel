﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace Propel.Ui
{
	public class BarIndicator : MonoBehaviour
	{
		[SerializeField]
		private Image barImage;
		[SerializeField]
		private float value;

		[SerializeField]
		private float lerpAmount = 0.75f;

		void Start()
		{

		}

		void Update()
		{
			this.barImage.fillAmount = Mathf.Lerp(this.barImage.fillAmount, value, lerpAmount);
		}

		public float Value
		{
			get {
				return this.value;
			}
			set {
				this.value = value;
			}
		}
	}
}

