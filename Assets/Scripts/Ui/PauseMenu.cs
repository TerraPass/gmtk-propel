﻿using UnityEngine;
using UnityEngine.UI;
using System;

using Terrapass.Extensions.Unity;

namespace Propel.Ui
{
	public class PauseMenu : MonoBehaviour
	{
		[SerializeField]
		private GameObject panel;

		[SerializeField]
		private Button restartButton;
		[SerializeField]
		private Button quitButton;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			restartButton.onClick.AddListener(() => Application.LoadLevel(Application.loadedLevel));
			quitButton.onClick.AddListener(() => Application.Quit());
		}

		public bool Visible
		{
			get {
				return this.panel.activeSelf;
			}
			set {
				this.panel.SetActive(value);
			}
		}
	}
}

